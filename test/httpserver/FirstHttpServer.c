/*
 ============================================================================
 N a m e        : FirstHttpServer.c
 A u t h o r      : Oleksandr Sotnikov
  :) Version     :
 Copyright   :
 D e s c r i p t i on : Simple HTTP server (JIRA TRLWO-776)
 ============================================================================
 */

#include <stdio.h>        /* perror() */
#include <sys/socket.h>   /* socket(), bind(), connect() */
#include <sys/wait.h>     /* waitpid() */
#include <arpa/inet.h>    /* sockaddr_in, inet_ntoa() */
#include <stdlib.h>       /* exit() */
#include <string.h>       /* memset() */
#include <unistd.h>       /* close(), fork() */

#include "debug.h"
#include "ServRequest.h"

#define HTTP_PORT 8080

#define MAXPENDING 20 /* Maximum outstanding connection requests */

int main(void) {

    int servSock;    /* Server socket */
    int clntSock;    /* Client socket */
    struct sockaddr_in servAddr; /* Server (local) address */
    struct sockaddr_in clntAddr; /* Client address */
    unsigned int clntAddrLen;    /* Length of client address */
    pid_t processID; /* Process ID from folk() */

    int optval;

    /* Create incoming TCP socket */
    if ((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        perror ("socket() failed");
        exit(1);
    }

    DEBUG_printf("Socket OK\n");

    /* Local address */
    memset(&servAddr, 0, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servAddr.sin_port = htons(HTTP_PORT);

    /* NOTE: Set reuse address (and port) to do easy debug process */
    optval = 1;
    setsockopt(servSock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    /* Bind to local address */
    if (bind(servSock,
             (struct sockaddr *) &servAddr,
             sizeof(servAddr)) < 0)
    {
        perror ("bind() failed");
        close (servSock);
        exit(1);
    }

    DEBUG_printf("Bind OK\n");

    /* Listen */
    if (listen(servSock, MAXPENDING) > 0) {
        perror ("listen() failed");
        close (servSock);
        exit(1);
    }

    DEBUG_printf("Listen OK\n");

    for (;;) /* FOREVER */
    {
        /* Set size if in-out parameter */
        clntAddrLen = sizeof(clntAddr);

        /* WAIT FOR A CLIENT */
        if ((clntSock = accept(servSock,
                               (struct sockaddr *) &clntAddr,
                               &clntAddrLen)) < 0)
        {
            perror ("accept() failed");
            close (servSock);
            exit(1);
        }

        DEBUG_printf("Accept OK\n");
        DEBUG_printf("Handling client %s, port %d (sock = %d) \n",
                     inet_ntoa(clntAddr.sin_addr),
                     ntohs(clntAddr.sin_port), clntSock);

        /* FORK child process */
        if ((processID = fork()) < 0)
        {
            perror ("fork() failed");
            close (servSock);
            close (clntSock);
            exit(1);
        }
        if (0 == processID)
        {
            /* Child process */
            close (servSock); /* Close legacy server socket */

            /* Service this request */
            HandleServRequest (clntSock);

            exit(0); /* Terminate child process */
        }

        /* Close client socket */
        close (clntSock);

        /* Clean up all child process zombies */
        for(;;) /* FOREVER */
        {
            processID = waitpid((pid_t)-1, NULL, WNOHANG);
            if (processID <= 0) /* No zombie to wait on */
                break;
        }

    }/* end of for (;;) */

    exit(0);
}

/* EOF */
