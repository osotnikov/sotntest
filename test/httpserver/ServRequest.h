/*
 ============================================================================
 Name        : ServRequest.c
 Author      : Oleksandr Sotnikov
 Version     :
 Copyright   :
 Description : Implemented only HTTP GET method.
 ============================================================================
 */

#ifndef SERVREQUEST_H_
#define SERVREQUEST_H_

/*
 * Function parses client request and sends response.
 * Limitations:
 *         - parses only GER request: looks for resource name and
 *           header "Connection" only;
 *         - checks resource on server folder and sends it in
 *           chunked-encoding.
 */
void HandleServRequest (int clntSock);

#endif /* SERVREQUEST_H_ */

/* EOF */

