/*
 ============================================================================
 Name        : ServRequest.c
 Author      : Oleksandr Sotnikov
 Version     :
 Copyright   :
 Description : Implemented only HTTP GET method.
 ============================================================================
 */

#include <stdio.h>        /* perror() */
#include <sys/socket.h>   /* socket(), bind(), connect() */
#include <stdlib.h>       /* exit() */
#include <string.h>       /* strlen(), memset() */
#include <unistd.h>       /* close() */
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

#include "debug.h"
#include "ParseHTTP.h"

/* SERVER PARAMETERS */
#define CHUNCKED_ENCODING 1
const char server_root[] = "/home/user/server/https";
const char defaultResource[] = "/hello.txt";


char resource   [PATH_MAX]; /* Resource name - received from client */
char resFileName[PATH_MAX]; /* File name with resource, full path name */

void WriteBuf (int sock, char * buf, int len);
void WriteLine (int sock, char * s);

/*
 * Function parses client request and sends response.
 * Limitations:
 *         - parses only GET request: looks for resource name and
 *           header "Connection" only;
 *         - checks resource on server folder and sends it in
 *           chunked-encoding.
 */
void HandleServRequest (int clntSock)
{
    ParseResult_t ParseResult;
    ConnectType_t ConnectType;
    int fd;
    int isResourceOk;

    /* Depending on "Connection" option, possible execute several
     * requests from client without closing */
    for(;;) /* FOREVER */
    {
        isResourceOk = 0; /* Default value */

        /* Get request */
        ParseResult = ParseGETReqOnly (clntSock, &ConnectType,
                                       resource, sizeof(resource));

        /* SEND RESPONSE */
        /* Initial line */
        if (GET_REQ == ParseResult)
        {
            DEBUG_printf("*GET request*\n");
            DEBUG_printf("*Resource name <%s>*\n", resource);
            DEBUG_printf("Connect type: =%d (0-Close, 1-keep-alive)\n", ConnectType);

            /* Check Resource */
            if (!strcmp(resource, "/")) /* Default resource */
                strcpy(resource, defaultResource);
            /* Create full file path */
            strcpy (resFileName, server_root);
            strcat (resFileName, resource);
            fd = open(resFileName, O_RDONLY);
            DEBUG_printf("Full file name =%s\n", resFileName);

            if (fd < 0) {
                WriteLine(clntSock, "HTTP/1.1 404 Not Found\r\n");
                DEBUG_printf("Send 404\n");
            }
            else {
                WriteLine(clntSock, "HTTP/1.1 200 OK\r\n");
                isResourceOk = 1;
            }
        }
        else /* Another than GET request or parser error */
        {
            /* Server Error Response*/
            WriteLine(clntSock,"HTTP/1.1 500 Internal Server Error\r\n");
            DEBUG_printf("Send 500\n");
        }

        /* Header */
        /* - Connection */
        if ((KEEPALIVE == ConnectType) && (PARSE_ERROR != ParseResult))
            WriteLine(clntSock, "Connection: keep-alive\r\n");
        else
            WriteLine(clntSock, "Connection: close\r\n");
        /* - Chuncked encoding */
        if (isResourceOk) {
#ifdef CHUNCKED_ENCODING
            WriteLine(clntSock, "Transfer-Encoding: chunked\r\n");
#endif
        }
        /* - Last string */
        WriteLine(clntSock, "\r\n");

        /* Resource */
        if (isResourceOk) {
#ifdef CHUNCKED_ENCODING
            char chkDataBuf[256]; /* Chunk data buffer */
            char chkSizeStr[6];   /* Chunk size string - HHH\r\n + \0 */
            int len;
            while((len = read(fd, chkDataBuf, sizeof(chkDataBuf))) > 0)
            {
                /* Size string */
                snprintf(chkSizeStr, sizeof(chkSizeStr), "%x\r\n", len);
                WriteLine(clntSock, chkSizeStr);
                /* Data */
                send(clntSock, chkDataBuf, len, 0);
                WriteLine(clntSock, "\r\n");
            }
            /* Last string */
            WriteLine(clntSock, "0\r\n\r\n");
#else
            int c;
            while(read(fd, &c, 1) == 1)
                send(clntSock, &c, 1, 0);
#endif
            close(fd);
        }

        if ((CLOSE == ConnectType) || (PARSE_ERROR == ParseResult))
        {
            close(clntSock);
            DEBUG_printf("pid=%d, sock=%d closed\n", getpid(), clntSock);
            return;
        }
    }/* End of for(;;) */
}

/* Function send a string to socket */
void WriteLine (int sock, char * s)
{
    WriteBuf(sock, s, strlen(s));
}

/* Function send a buffer to socket */
void WriteBuf (int sock, char * buf, int len)
{
    if(send(sock, buf, len, 0) < 0) {
        perror ("send() failed");
        close(sock);
        exit(1);
    }
}

/* EOF */

