/*
 * debug.c
 */
#include <stdio.h>
#include <stdarg.h>

#define DEBUG_TRACE 1

void DEBUG_printf(char* format, ...)
{
#ifdef DEBUG_TRACE
    va_list args;
    va_start(args, format);

    vprintf(format, args);

    va_end(args);
#endif
}


/*EOF*/
