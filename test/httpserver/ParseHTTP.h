/*
 ============================================================================
 Name        : ParseHTTP.c
 Author      : Oleksandr Sotnikov
 Version     :
 Copyright   :
 Description : Implemented only HTTP GET method.
 ============================================================================
 */

#ifndef PARSEHTTP_H_
#define PARSEHTTP_H_

/* Parse result */
typedef enum {PARSE_ERROR=-1, GET_REQ, UNKNOWN_REQ} ParseResult_t;

/* Connection Type */
typedef enum {CLOSE, KEEPALIVE} ConnectType_t;

/*
 * Function receives and parses client request.
 * Parses only GET request.
 * Return resource name and connection type.
 */
ParseResult_t ParseGETReqOnly (int sock,
                               ConnectType_t * pConnType,
                               char * resource,
                               int    resourceLen);


#endif /* PARSEHTTP_H_ */

/* EOF */

