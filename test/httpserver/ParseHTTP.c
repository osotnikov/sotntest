/*
 ============================================================================
 Name        : ParseHTTP.c
 Author      : Oleksandr Sotnikov
 Version     :
 Copyright   :
 Description : Implemented only HTTP GET method.
 ============================================================================
 */

#include <stdio.h>        /* perror() */
#include <stdlib.h>       /* exit() */
#include <string.h>       /* strlen(), memset() */
#include <ctype.h>        /* isspace() */
#include <unistd.h>       /* close() */
#include <sys/socket.h>

#include "debug.h"
#include "ParseHTTP.h"

int ReadLine(int sock, char *buf, int bufLen);

#define MAX_REQ_LINE 1024
char reqLine[MAX_REQ_LINE]; /* Request line buffer */

/*
 * Function receives and parses client request.
 * Parses only GET request.
 * Gives back resource name and connection type.
 */
ParseResult_t ParseGETReqOnly (int sock,
                               ConnectType_t * pConnType,
                               char * resource,
                               int    resourceLen)
{
    ParseResult_t result;
    char *pBuf;

    ConnectType_t connType = CLOSE; /* by default */

    /* INITIAL LINE */
    if (ReadLine(sock, reqLine, sizeof(reqLine)) < 0) {
        DEBUG_printf("Error reading initial line in the request\n");
        result = PARSE_ERROR;
    }
    else {
        DEBUG_printf ("LINE: %s [%d]\n", reqLine, strlen(reqLine));

        /* Check only GET request */
        pBuf = reqLine;
        if (!strncmp(pBuf, "GET ", 4))
        {
            result = GET_REQ;
            pBuf += 4; /* Add "GET " size */

            /* Read resource */
            /*  - skip spaces */
            while ( *pBuf && isspace(*pBuf) )
                pBuf++;
            /*  - check '/' */
            if (*pBuf != '/') {
                DEBUG_printf("Resource without \"/\".\n");
                result = PARSE_ERROR;
            }
            else {
                /* Find the end of resource */
                char *p   = strchr(pBuf, ' ');
                if (p == NULL) {
                    DEBUG_printf("Absent space symbol after resource.\n");
                    result = PARSE_ERROR;
                }
                else {
                    int len = p - pBuf;
                    /* If need to cut off the length */
                    len = (len < (resourceLen-1)) ? len : (resourceLen-1);
                    memcpy(resource, pBuf, len);
                    resource[len] = '\0';
                }
            }
        }
        else {
           result = UNKNOWN_REQ;
        }
    }

    /* HEADER */
    for (;;) /* FOREVER */
    {
        if ( ReadLine(sock, reqLine, sizeof(reqLine)) < 0) {
            DEBUG_printf("Error reading line from request\n");
            exit(1);
        }

        DEBUG_printf ("LINE: %s [%d]\n", reqLine, strlen(reqLine));

        /* Header case-insensitive */
        char * p = reqLine;
        for ( ; *p; ++p) *p = tolower(*p); /* Translate to lower chars */

        /* CHECK CONNECT OPTION */
        pBuf = reqLine;
        if (!strncmp(pBuf, "connection:", 11))
        {
            pBuf += 11; /* Add size of "connection:" */
            /*  - skip spaces */
            while ( *pBuf && isspace(*pBuf) )
                pBuf++;

            if (!strncmp(pBuf, "keep-alive", 10)) {
                connType = KEEPALIVE;
            }
        }

        if (strlen(reqLine) == 0) {/* Last line */
            DEBUG_printf("Last line \n");
            break; /* GO OUT FOREVER LOOP => */
        }
    }/* End of for(;;) */

    if (pConnType != NULL)
        *pConnType = connType;

    return result;
}

/* Read a line from client request   */
/* 0 - success, -1 (or other value) - readError */
int ReadLine(int sock, char *buf, int bufLen)
{
    char c;
    int  rc;
    int i = 0;

    /* Read by one char */
    for (;;) /* FOREVER */
    {
        if ((rc = recv(sock, &c, 1, 0)) < 0) {
            perror("read() failed");
            return -1; /* Read error */
        }
        else if (rc == 0)
        {
            DEBUG_printf("recv() result is 0\n");
            /* This is shut down from client for blocking socket */
            /* Close connection */
            close(sock);
            /* Exit from client process */
            exit(1);
        }

        if (i >= (bufLen-1))
            return -1; /* Line is out of max size */

        buf[i++] = c;  /* Store a char */

        if ('\n' == c) { /* Check end of Line */
            buf[i] = '\0';
            /* Skip CRLF (also possible only LF) */
            buf[i-1] = '\0'; /* LF */
            if ((strlen(buf) >= 1) && ('\r' == buf[i-2]))
                buf[i-2] = '\0';

            return 0; /* Success */
        }
    }
}

/* EOF */

